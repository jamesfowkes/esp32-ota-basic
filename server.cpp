#include <WiFi.h>
#include <WiFiClient.h>
#include <WebServer.h>
#include <HTTPClient.h>
#include <ArduinoJson.h>

#include "server.h"

static WebServer s_server(80);
static char const * const HTML =    
"<html>\n"
"    <head>\n"

"          <meta http-equiv='refresh' content='1;url=/index''>\n"
"        <title>ESP32 OTA Update</title>\n"
"        <style>\n"
"           html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}\n"
"        </style>\n"
"    </head>\n"
"    <body>\n"
"        <h1>ESP32 OTA Update</h1>\n"
"        <a href=\"/webota\">Go to web OTA page</a>\n"
"    </body>\n"
"</html>";

static char s_buffer[2048];

static void print_args(WebServer& server)
{
    String dbg = "/:";
    dbg += server.args();
    dbg += " args: ";
    for (uint8_t i = 0; i < server.args(); i++) {
        dbg += " " + server.argName(i) + ": " + server.arg(i) + ", ";
    }
    Serial.println(dbg);
}

void render_page(bool refresh)
{
    strcpy(s_buffer, HTML);
}

void returnOK() {
  s_server.send(200, "text/plain", "");
}

void handle_index()
{
    print_args(s_server);

    render_page(s_server.args() > 0);
    s_server.sendHeader("Cache-Control", "no-cache");
    s_server.send(200, "text/html", s_buffer);
}
void server_start()
{
    s_server.on("/index", HTTP_GET, handle_index);
    s_server.begin();
}

void server_loop()
{
    s_server.handleClient();
}
