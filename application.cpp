#include <TaskAction.h>
#include <esp_system.h>
#include <EEPROM.h>

#include "app-wifi.h"
#include "wifi-control.h"
#include "serial-handler.h"
#include "server.h"
#include "application.h"

static const uint8_t LED_PIN = 2;

static void heartbeat_task_fn(TaskAction* this_task)
{
    (void)this_task;
    static bool led_on = false;

    led_on = !led_on;
    digitalWrite(LED_PIN, led_on);
}
static TaskAction s_heartbeat_task(heartbeat_task_fn, 150, INFINITE_TICKS);

void application_setup()
{
    pinMode(LED_PIN, OUTPUT);
}

void application_loop()
{
    s_heartbeat_task.tick();
}
