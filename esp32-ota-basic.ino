#include <Esp.h>
#include <TaskAction.h>
#include <esp_system.h>
#include <EEPROM.h>
#include <WebOTA.h>

#include "app-wifi.h"
#include "wifi-control.h"
#include "serial-handler.h"
#include "server.h"
#include "application.h"

void setup()
{
    EEPROM.begin(32);
    Serial.begin(115200);
    Serial.setDebugOutput(true);
    esp_log_level_set("*", ESP_LOG_VERBOSE);

    wifi_control_setup();
    app_wifi_setup();
    application_setup();

    delay(3000);
}

void loop()
{
    wifi_control_loop();
    serial_loop();
    server_loop();
    application_loop();
    
    if (wifi_control_check_and_clear())
    {
        Serial.println("Clearing WiFi credentials");
        app_wifi_wipe_credentials();
        delay(3000);
        Serial.println("Restarting...");
        ESP.restart();
        delay(1000);
    }

    webota.handle();
    
}
